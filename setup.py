from setuptools import setup, find_packages

# get description from readme file
with open('README.md', 'r') as f:
    long_description = f.read()

# setup
setup(
    name='IRL',
    version='V0.1',
    description='',
    long_description=long_description,
    long_description_content_type="text/markdown",
    author='You Zhou',
    author_email='derekyou.zhou@gmail.com',
    maintainer=' ',
    maintainer_email='',
    license=' ',
    url=' ',
    platforms=[''],
    packages=find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3.8.3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: POSIX :: Linux",
    ],
)
