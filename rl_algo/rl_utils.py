import numpy as np
from torch.utils.data import Dataset
import torch
from irl_utils import prepare_torch, to_tensor
import torch.nn as nn
import os
from torch.distributions import Normal
device = prepare_torch()


class RLExperience(Dataset):
    def __init__(self, gamma=0.99, gae=0.97, device=device):
        super(RLExperience, self).__init__()
        self.states = []
        self.actions = []
        self.rewards = []
        self.dones = []
        self.probs = []
        self.values = []
        self.device = device
        self.gamma = gamma
        self.gae = gae

    def remember(self, state, action, reward, done, prob, value):
        self.states.append(state)
        self.actions.append(action)
        self.rewards.append(reward)
        if done:
            self.dones.append(np.array([1.0]))
        else:
            self.dones.append(np.array([0.0]))

        self.probs.append(prob)
        self.values.append(value)

    def clear(self):
        self.states = []
        self.actions = []
        self.rewards = []
        self.dones = []
        self.probs = []
        self.values = []
        self.advantages = []

    def process(self):
        self.states = np.stack(self.states)
        self.actions = np.stack(self.actions)
        self.rewards = np.array(self.rewards)
        self.rewards = np.expand_dims(self.rewards, axis=-1)
        self.dones = np.array(self.dones)
        self.probs = np.array(self.probs)
        self.values = np.array(self.values)

    def calculate_advantage(self, last_v, done):
        buffer_size = len(self.rewards)
        self.advantages = np.zeros((buffer_size,), dtype=np.float32)

        last_gae_lam = 0
        for step in reversed(range(buffer_size)):
            if step == buffer_size - 1:
                next_non_terminal = 1.0 - done
                next_values = last_v
            else:
                next_non_terminal = 1.0 - self.dones[step + 1]
                next_values = self.values[step + 1]

            # print('nv: {}'.format(next_values))
            # print('nt: {}'.format(next_non_terminal))

            delta = self.rewards[step] + self.gamma * next_values * next_non_terminal - self.values[step]
            last_gae_lam = delta + self.gamma * self.gae * next_non_terminal * last_gae_lam
            self.advantages[step] = last_gae_lam

        self.states = np.stack(self.states)
        self.actions = np.stack(self.actions)
        self.rewards = np.array(self.rewards)
        self.rewards = np.expand_dims(self.rewards, axis=-1)
        self.dones = np.array(self.dones)
        self.probs = np.array(self.probs)
        self.values = np.array(self.values)
        self.advantages = np.expand_dims(self.advantages, axis=-1)

    def __len__(self):
        return len(self.states)

    def __getitem__(self, item):
        state = self.states[item, :]
        action = self.actions[item,:]
        prob = self.probs[item,:]

        done = self.dones[item,:]
        value = self.values[item,:]
        reward = self.rewards[item,:]
        advantage = self.advantages[item, :]

        cstate = torch.from_numpy(state).float().to(self.device)
        caction = torch.from_numpy(action).float().to(self.device)
        creward = torch.from_numpy(reward).float().to(self.device)
        cdone = torch.from_numpy(done).float().to(self.device)
        cprob = torch.from_numpy(prob).float().to(self.device)
        cvalue = torch.from_numpy(value).float().to(self.device)
        cadvantage = torch.from_numpy(advantage).float().to(self.device)
        return cstate, caction, creward, cdone, cprob, cvalue, cadvantage



class DisDataset(Dataset):
    def __init__(self, state_a, action_a, state_e, action_e):
        super(DisDataset, self).__init__()
        self.state_a = state_a
        self.action_a = action_a
        self.state_e = state_e
        self.action_e = action_e

    def __len__(self):
        return len(self.state_a)

    def __getitem__(self, item):
        state_a = self.state_a[item,:]
        action_a = self.action_a[item,:]
        state_e = self.state_e[item,:]
        action_e = self.action_e[item,:]
        return to_tensor(state_a), to_tensor(action_a), to_tensor(state_e), to_tensor(action_e)


class ActorNet(nn.Module):
    def __init__(self, model, feat_extractor=None,  device=device, ckpt_dir='ppo_model', log_std_init=0.0):
        super(ActorNet, self).__init__()
        self.model = model
        self.device = device
        self.model.to(device)
        self.ckpt_f = os.path.join(ckpt_dir, 'actor_net')
        self.log_std = nn.Parameter(torch.ones(model.out_dim) * log_std_init, requires_grad=True).to(device)
        self.feat_extractor = feat_extractor

    def forward(self, input):
        if self.feat_extractor:
            input = self.feat_extractor.forward(input)

        z_mean = self.model.forward(input)
        z_mean = z_mean.squeeze(dim=0)
        return z_mean

    def act_on(self, input):
        z_mean = self.forward(input)
        z_std = torch.ones_like(z_mean) * self.log_std.exp()
        dist = Normal(z_mean, z_std)
        return dist, dist.entropy()

    def save(self):
        torch.save(self.state_dict(), self.ckpt_f)

    def load(self):
        self.load_state_dict(torch.load(self.ckpt_f))


class CriticNet(nn.Module):
    def __init__(self, model, feat_extractor=None, device=device, ckpt_dir='ppo_model'):
        super(CriticNet, self).__init__()
        self.model = model
        self.device = device
        self.model.to(device)
        self.ckpt_f = os.path.join(ckpt_dir, 'critic_net')
        self.feat_extractor = feat_extractor

    def forward(self, state):
        if self.feat_extractor:
            state = self.feat_extractor.forward(state)

        if len(state.shape) < 2:
            state = state.unsqueeze(dim=0)

        value = self.model.forward(state)

        return value.squeeze(dim=0)

    def save(self):
        torch.save(self.state_dict(), self.ckpt_f)

    def load(self):
        self.load_state_dict(torch.load(self.ckpt_f))