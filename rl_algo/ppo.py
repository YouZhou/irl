import numpy as np
import torch
import torch.nn as nn
import torch.optim as optim
from rl_algo.rl_utils import RLExperience
from torch.distributions import Normal
import os
from torch.utils.data import DataLoader, RandomSampler, BatchSampler
from rl_algo.agent import Agent
from irl_utils import prepare_torch, to_tensor, to_numpy
device = prepare_torch()


class PPO(Agent):
    def __init__(self, actor, critic, ent_loss_coeff=0.01,
                 gamma=0.99, gae=0.95, ppo_clip=0.2, lr=0.0003, critic_loss_coeff=0.5, ckpt_f='ppo_model/test',
                 max_grad_norm=0.5, verbose=False, init_explore_noise=0.0, n_explore_iter=500, device=device):
        super(PPO, self).__init__(ckpt_f)
        self.memory = RLExperience(device=device, gamma=gamma, gae=gae)
        self.actor = actor
        self.critic = critic
        self.actor.to(device)
        self.critic.to(device)
        self.gamma = gamma
        self.ppo_clip = ppo_clip

        self.critic_loss_coeff = critic_loss_coeff
        self.ent_loss_coeff = ent_loss_coeff
        self.opt = optim.Adam(self.parameters(), lr=lr)
        self.ckpt_f = ckpt_f
        self.max_grad_norm = max_grad_norm
        self.verbose = verbose
        self.learn_score_hist = []
        self.learn_steps_hist = []
        self.learn_hist = []
        self.n_explore_iter = n_explore_iter
        self.init_explore_noise = init_explore_noise

    def act(self, state, deterministic=False, noise=0):
        with torch.no_grad():
            if len(state.shape) < 2:
                state = state.unsqueeze(dim=0)

            if not deterministic:
                dist, entropy = self.actor.act_on(state)
                action = dist.sample()
                noise_dist = Normal(torch.zeros_like(dist.mean), torch.ones_like(dist.mean) * noise)
                noise = noise_dist.sample()
                action = action + noise
                prob = dist.log_prob(action.squeeze())
            else:
                action = self.actor.forward(state)
                prob = torch.tensor([1.0])

            return action, prob


    def learn(self, n_epochs, batch_size=5, verbose=False):
        data_sampler = BatchSampler(RandomSampler(self.memory), batch_size=batch_size, drop_last=False)
        dataloader = DataLoader(self.memory, sampler=data_sampler)

        for i in range(n_epochs):
            epoch_actor_loss = 0
            epoch_entropy_loss = 0
            epoch_critic_loss = 0
            count = 0
            for _, (state, action, reward, done, prob, value, advantage) in enumerate(dataloader):
                state = state.squeeze(dim=0)
                action = action.squeeze(dim=0)
                prob = prob.squeeze(dim=0)
                value = value.squeeze(dim=0)
                advantage = advantage.squeeze(dim=0)

                advantage = (advantage - advantage.mean()) / (advantage.std() + 1e-8)

                dist, entropy = self.actor.act_on(state)
                c_val = self.critic.forward(state)
                n_probs = dist.log_prob(action)
                p_ratio = torch.exp(n_probs - prob)
                loss_1 = advantage * p_ratio
                loss_2 = torch.clamp(p_ratio, 1-self.ppo_clip, 1+self.ppo_clip) * advantage
                actor_loss = - torch.min(loss_1, loss_2).mean()

                returns = advantage + value
                critic_loss = nn.MSELoss()(returns, c_val)
                entropy_loss = - torch.mean(entropy)
                loss = actor_loss + self.ent_loss_coeff * entropy_loss + self.critic_loss_coeff * critic_loss

                self.opt.zero_grad()
                loss.backward()

                torch.nn.utils.clip_grad_norm_(self.parameters(), self.max_grad_norm)
                self.opt.step()
                epoch_actor_loss += actor_loss
                epoch_entropy_loss += entropy_loss
                epoch_critic_loss += critic_loss
                count += 1

            if verbose:
                print('epoch: %1d,, actor_loss: %.10f, critic_loss: %.10f, entropy_loss: %.5f' %
                      (i, epoch_actor_loss/count, epoch_critic_loss/count, epoch_entropy_loss/count), end='\r')

        if verbose:
            print('epoch: %1d,, actor_loss: %.10f, critic_loss: %.10f, entropy_loss: %.5f' %
                  (i, epoch_actor_loss / count, epoch_critic_loss / count, epoch_entropy_loss / count))

        self.memory.clear()


    def rollouts(self, env, rollout_steps, explore_noise=0):
        score = 0
        obs = env.reset()
        done = False
        last_done = done
        for n_steps in range(rollout_steps):
            action, prob = self.act(to_tensor(obs), noise=explore_noise)
            value = self.critic(to_tensor(obs))

            action = to_numpy(action)
            prob = to_numpy(prob)
            value = to_numpy(value)

            # action = np.clip(action, env.action_space.low, env.action_space.high)
            # action = np.clip(action, env.action_space[0], env.action_space[1])

            if np.isnan(action).any():
                print("Encounter 'nan' action ... aborting ...")
                return False

            obs_, reward, done, _ = env.step(action)

            env.render()
            score += reward
            self.memory.remember(obs, action, reward, last_done, prob, value)
            obs = obs_
            last_done = done
            self.total_steps += 1

            if done:
                obs = env.reset()
                self.learn_score_hist.append(score)
                self.learn_steps_hist.append(self.total_steps)
                score = 0

        self.memory.calculate_advantage(last_v=value, done=done)
        return True

    def train_policy(self, env, num_iter, rollout_steps, learn_epochs_per_iter, learn_batch_size, stop_criterion=0):
        if self.load():
            print('Found the old model, continue training it')
        else:
            print('Not found the old model, training ...')

        self.learn_score_hist = []
        self.learn_steps_hist = []
        self.record_score_hist = []
        self.record_steps_hist = []
        self.total_steps = 0
        for iter in range(num_iter):
            if iter % self.n_explore_iter == 0:
                explore_noise = self.init_explore_noise
            else:
                explore_noise = 0

            if not self.rollouts(env=env, rollout_steps=rollout_steps, explore_noise=explore_noise):
                return False

            self.learn(n_epochs=learn_epochs_per_iter,  batch_size=learn_batch_size)
            avg_score = np.mean(self.learn_score_hist[-100:])
            self.record_score_hist.append(avg_score)
            self.record_steps_hist.append(self.total_steps)
            if iter % 20 == 0:
                self.save()

            print('iter: %1d, total_steps: %1d, avg_score: %.10f' % (iter, self.total_steps, avg_score), end='\r')

            if avg_score is None or avg_score > stop_criterion:
                break

        print('iter: %1d, total_steps: %1d, avg_score: %.10f' % (iter, self.total_steps, avg_score))
        self.learn_hist = np.stack([np.array(self.record_steps_hist), np.array(self.record_score_hist)], axis=-1)
        return True


if __name__ == '__main__':
    from basic_models import SimpleNN
    from envs.envs import PendulumEnv
    from rl_algo.rl_utils import ActorNet, CriticNet

    env = PendulumEnv()

    # import gym
    # env = gym.make("Pendulum-v0")

    actor = ActorNet(model=SimpleNN(in_dim=3, out_dim=1, n_layers=(64, 64)))
    critic = CriticNet(model=SimpleNN(in_dim=3, out_dim=1, n_layers=(64, 64)))
    ppo = PPO(actor=actor, critic=critic, ckpt_f='ppo_model/expert')
    ppo.train_policy(env=env, num_iter=1000, rollout_steps=1024, learn_epochs_per_iter=20, learn_batch_size=200)