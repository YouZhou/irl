import torch
import os
import torch.nn as nn


class Agent(nn.Module):
    def __init__(self, ckpt_f):
        super(Agent, self).__init__()
        self.ckpt_f = ckpt_f

    def act(self, state, deterministic=False, noise=0):
        pass

    def save(self):
        torch.save(self.state_dict(), self.ckpt_f)

    def load(self):
        if os.path.exists(self.ckpt_f):
            self.load_state_dict(torch.load(self.ckpt_f))
            return True
        else:
            return False


