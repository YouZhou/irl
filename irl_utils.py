import torch
import numpy as np

def prepare_torch():
    use_cuda = torch.cuda.is_available()
    device = "cpu" #torch.device("cuda:0" if use_cuda else "cpu")
    torch.set_default_dtype(torch.float32)
    return device


device = prepare_torch()


def to_numpy(t : torch.Tensor):
    return t.detach().cpu().float().numpy()


def to_tensor(n : np.ndarray, device=device):
    return torch.from_numpy(n).float().to(device)
