from rewards import *

class Env():
    def __init__(self, reward):
        self.reward = reward

    def sys(self, s, a):
        pass

    def prob(self, s_1, s_0, a_0):
        pass

    def f(self, s, a):
        pass

    def f_T(self, s):
        return self.f(s, 0)

    def f_traj(self, traj_s, traj_a):
        """
        :param traj_s: timestamps x traj_dim
        :param traj_a: (timestamps-1) x action_dim
        :return: f_traj: timestamps x feat_dim -> feature trajectory
        """
        f_traj = [self.f(traj_s[i,:], traj_a[i,:]) for i in range(len(traj_a))]
        f_traj.append(self.f_T(traj_s[-1,:]))
        return np.stack(f_traj)

    def f_trajs(self, trajs_s, trajs_a):
        """
        :param trajs_s: num_demo x timestamps x traj_dim
        :param trajs_a: num_demo x (timestamps-1) x action_dim
        :return: f_trajs: num_demo x timestamps x feat_dim -> feature trajectory
        """
        f_trajs = []
        num_demo = np.shape(trajs_s)[0]
        for i in range(num_demo):
            f_traj = self.f_traj(trajs_s[i,:,:], trajs_a[i,:,:])
            f_trajs.append(f_traj)

        return np.stack(f_trajs)



    def value(self, traj_s, traj_a):
        f_traj = self.f_traj(traj_s, traj_a)
        rewards = self.reward.forward(f_traj)
        return np.sum(rewards)


class Simple2DGridEnv(Env):
    def __init__(self,  reward, timelength, action_set, gridsize=10):
        super(Simple2DGridEnv, self).__init__(reward)
        self.state_dim = 2
        self.action_dim = 2
        self.feat_dim = 2
        self.action_set = action_set
        self.timelength = timelength
        xv, yv = np.meshgrid(range(10), range(10))
        state_set = []
        for i in range(gridsize):
            for j in range(gridsize):
                state_set.append(np.array([xv[i,j],yv[i,j]]))

        self.state_set = state_set

    def sys(self, s, a):
        return s + a

    def expect_s(self, s_0, a_0, val):
        s_1 = np.array([self.prob(s_1, s_0, a_0) for s_1 in self.state_set])
        return np.dot(s_1, val)

    def f(self, s, a=None):
        return s

    def prob(self, s_1, s_0, a_0):
        if np.all(s_1 == s_0 + a_0):
            return 1
        else:
            return 0

    def get_expert_trajs(self, goal=np.array([5.0,5.0], dtype=np.float64),  num_traj=10):
        back_actions = [-action for action in self.action_set]
        trajs_s = []
        trajs_a = []
        for ntraj in range(num_traj):
            curr_s = goal
            traj_s = [curr_s]
            traj_a = []
            for i in range(self.timelength-1):
                rnd_ind = np.random.randint(0, len(self.action_set), 1)
                back_action = back_actions[rnd_ind[0]]
                curr_s = curr_s + back_action
                traj_s.append(curr_s)
                traj_a.append(-back_action)

            traj_s.reverse()
            traj_a.reverse()
            traj_s = np.stack(traj_s)
            traj_a = np.stack(traj_a)
            trajs_s.append(traj_s)
            trajs_a.append(traj_a)


        return np.stack(trajs_s), np.stack(trajs_a)

