import numpy as np
import torch
import torch.nn as nn
from torch import optim
use_cuda = torch.cuda.is_available()
device = torch.device("cuda:0" if use_cuda else "cpu")
torch.set_default_dtype(torch.float64)


class LinearReward(nn.Module):
    def __init__(self, feat_dim, lrate=0.0001):
        super(LinearReward, self).__init__()
        self.feat_dim = feat_dim
        self.linear = nn.Linear(feat_dim, 1, bias=False)
        self.linear.to(device)
        self.optimizer = optim.Adam(self.parameters(), lr=lrate)

    def evaluate(self, feat: np.ndarray):
        if len(np.shape(feat)) < 2:
            feat = np.expand_dims(feat,axis=0)

        if np.shape(feat)[1] != self.feat_dim:
            raise ValueError('Feature dimension {} is different from the required one {}'.format(np.shape(feat)[1],
                                                                                                 self.feat_dim))
        with torch.no_grad():
            rewards = self.forward(feat)
            return rewards.cpu().numpy()

    def forward(self, feat: np.ndarray):
        """
        :param feat: [batch_size x feature_dim]
        :param a: None
        :return: reward: [batch_size x 1]
        """
        if len(np.shape(feat)) < 2:
            feat = np.expand_dims(feat,axis=0)

        if np.shape(feat)[1] != self.feat_dim:
            raise ValueError('Feature dimension {} is different from the required one {}'.format(np.shape(feat)[1], self.feat_dim))

        feat = torch.from_numpy(feat)
        feat = feat.type(torch.DoubleTensor).to(device)
        rewards = self.linear(feat)
        rewards = rewards - torch.max(rewards)
        return rewards


class SimpleNNReward(nn.Module):
    def __init__(self, feat_dim, layers=None, lrate=0.0001):
        super(SimpleNNReward, self).__init__()
        if layers is None:
            layers = [10,10,1]

        self.feat_dim = feat_dim
        self.layers = []
        in_dim = feat_dim
        for i in range(len(layers)):
            self.layers.append(nn.Linear(in_dim, layers[i]))
            self.layers.append(nn.LeakyReLU())
            in_dim = layers[i]

        self.layers = nn.ModuleList(self.layers)
        self.to(device)
        self.optimizer = optim.Adam(self.parameters(), lr=lrate)

    def evaluate(self, feat: np.ndarray):
        if len(np.shape(feat)) < 2:
            feat = np.expand_dims(feat,axis=0)

        if np.shape(feat)[1] != self.feat_dim:
            raise ValueError('Feature dimension {} is different from the required one {}'.format(np.shape(feat)[1],
                                                                                                 self.feat_dim))
        with torch.no_grad():
            rewards = self.forward(feat)
            return rewards.cpu().numpy()

    def forward(self, feat: np.ndarray):
        """
        :param feat: [batch_size x feature_dim]
        :param a: None
        :return: reward: [batch_size x 1]
        """
        if len(np.shape(feat)) < 2:
            feat = np.expand_dims(feat,axis=0)

        if np.shape(feat)[1] != self.feat_dim:
            raise ValueError('Feature dimension {} is different from the required one {}'.format(np.shape(feat)[1], self.feat_dim))

        feat = torch.from_numpy(feat)
        feat = feat.type(torch.DoubleTensor).to(device)
        out = feat
        for layer in self.layers:
            out = layer(out)

        rewards = out - torch.max(out)
        return rewards


if __name__ == "__main__":
    rfcn = LinearReward(feat_dim=10)
    rewards = rfcn.evaluate(np.ones(shape=(2,10)))
    print(rewards)