import torch
import torch.nn as nn
from irl_utils import prepare_torch, to_numpy, to_tensor
from rl_algo.ppo import PPO
import torch.optim as optim
from basic_models import SimpleNN
from rl_algo.rl_utils import DisDataset
from torch.utils.data import DataLoader
from rl_algo.rl_utils import ActorNet, CriticNet
from envs.envs import PendulumEnv
import numpy as np

device = prepare_torch()

class GAIL(nn.Module):
    def __init__(self, env, agent : PPO, expert : PPO, discriminator, n_rollouts_steps=1024):
        super(GAIL, self).__init__()
        self.env = env
        self.agent = agent
        self.expert = expert
        self.discriminator = discriminator
        self.total_steps = 0
        self.n_rollouts_steps = n_rollouts_steps

    def rollouts(self, n_rollouts_steps, horizon=float('inf'), explore_noise=0.0, test=False, render=False):
        score = 0
        obs = self.env.reset()
        done = False
        last_done = done

        obs_a = obs.copy()
        obs_e = obs.copy()

        self.agent.memory.clear()
        self.expert.memory.clear()
        total_score = 0
        n_epochs = 0
        steps = 0
        self.avg_reward = 0
        self.avg_exp_reward = 0
        self.avg_dis_reward = 0
        for n_steps in range(n_rollouts_steps):
            ## collect agent data
            if test:
                action, prob = self.agent.act(to_tensor(obs_a), True)
                score += self.env.get_reward(obs_a, to_numpy(action))
                if render:
                    self.env.state = self.env.get_state(obs_a)
                    self.env.last_u = to_numpy(action)
                    self.env.render()
            else:
                action, prob = self.agent.act(to_tensor(obs_a), noise=explore_noise)
                self.total_steps += 1

            self.avg_reward += self.env.get_reward(obs_a, to_numpy(action))

            ### collect agent data
            value = self.agent.critic(to_tensor(obs_a))
            # reward = self.env.get_reward(obs_a, to_numpy(action)) #
            reward = - torch.log(self.discriminator(torch.cat([to_tensor(obs_a), action], dim=-1)) + 1e-10)
            reward = to_numpy(reward)

            self.avg_dis_reward += reward

            self.agent.memory.remember(obs_a, to_numpy(action), reward , last_done, to_numpy(prob), to_numpy(value))
            state_a = self.env.sysfun(self.env.get_state(obs_a), to_numpy(action))
            obs_a = self.env._get_obs(state_a)

            ### collect expert data
            act_e, _ = self.expert.act(to_tensor(obs_e), True)
            self.avg_exp_reward += self.env.get_reward(obs_e, to_numpy(act_e))
            self.expert.memory.remember(obs_e, to_numpy(act_e), 0, 0, 0, 0)
            state_e = self.env.sysfun(self.env.get_state(obs_e), to_numpy(act_e))
            obs_e = self.env._get_obs(state_e)

            ## Horizon
            if n_steps != 0 and n_steps % horizon == 0:
                done = True
                obs_a = obs_e.copy()
            else:
                steps += 1
                done = self.env.is_done(steps, obs_a)

            last_done = done
            if done:
                n_epochs += 1
                total_score += score
                score = 0
                steps = 0
                obs = self.env.reset()
                obs_a = obs.copy()
                obs_e = obs.copy()

        self.avg_reward /= n_rollouts_steps
        self.avg_dis_reward /= n_rollouts_steps
        self.avg_exp_reward /= n_rollouts_steps
        if not test:
            self.agent.memory.calculate_advantage(last_v=to_numpy(value), done=done)
            self.expert.memory.process()

        return total_score / n_epochs

    def train_discriminator(self, train_dis_steps, lr_dis=1e-3, verbose=False, batch_size=100):
        opt_dis = optim.Adam(self.discriminator.parameters(), lr=lr_dis)
        dataset = DisDataset(self.agent.memory.states, self.agent.memory.actions, self.expert.memory.states,
                             self.expert.memory.actions)
        dataloader = DataLoader(dataset, batch_size=batch_size)
        for i in range(train_dis_steps):
            avg_cost = 0
            for count, (state_a, action_a, state_e, action_e) in enumerate(dataloader):
                opt_dis.zero_grad()
                dis_a = self.discriminator(torch.cat([state_a, action_a], dim=-1))
                dis_e = self.discriminator(torch.cat([state_e, action_e], dim=-1))

                score_a = torch.mean(torch.log(dis_a + 1e-10)) #nn.BCELoss()(dis_a, torch.zeros((dis_a.shape[0], 1), device=device))
                score_e = torch.mean(torch.log((1-dis_e) + 1e-10))#nn.BCELoss()(dis_e, torch.ones((dis_e.shape[0], 1), device=device))
                cost = - (score_a + score_e)
                cost.backward()
                opt_dis.step()
                avg_cost += cost

            if verbose:
                print('train_dis ===> epoch: %1d,, dis_cost: %.10f' % (i, avg_cost / count), end='\r')

        if verbose:
            print('train_dis ===> epoch: %1d,, dis_cost: %.10f' % (i, avg_cost / count))

    def train_model(self, max_epochs, train_dis_steps, train_policy_steps, lr_dis=1e-3, verbose=False, horizon=float('inf')):
        train_hist = np.zeros((1, max_epochs))
        for i in range(max_epochs):
            ### collect data
            if i % 500 == 0:
                explore_noise = 1.0
            else:
                explore_noise = 0.0

            self.rollouts(self.n_rollouts_steps, horizon=horizon, explore_noise=explore_noise)
            ### train discriminator
            self.train_discriminator(train_dis_steps=train_dis_steps, lr_dis=lr_dis, verbose=False)
            ### train policy
            if i != 0:
                self.agent.learn(n_epochs=train_policy_steps, batch_size=100, verbose=False)

            if i % 50 == 0:
                horizon += 10
                self.agent.save()

            # score = self.rollouts(400, horizon=float('inf'), test=True)
            if self.avg_reward is None:
                break

            train_hist[0,i] = self.avg_reward
            print("epoch: %1d, steps: %1d, avg_reward: %.5f, exp_avg_reward: %.5f, dis_avg_reward: %.5f"
                  % (i, self.total_steps, self.avg_reward, self.avg_exp_reward, self.avg_dis_reward), end='\r')

        return train_hist


import click
@click.command()
@click.option("--hgail", default=0, help="1: hgail, 0: gail")
def experiment(hgail):
    env = PendulumEnv()
    expert = PPO(actor=ActorNet(model=SimpleNN(in_dim=3, out_dim=1, n_layers=(64, 64))),
                 critic=CriticNet(model=SimpleNN(in_dim=3, out_dim=1, n_layers=(64, 64))),
                 ckpt_f='../rl_algo/ppo_model/expert')
    expert.load()
    print(expert)
    n_exps = 100

    if hgail:
        name = 'hgail'
        horizon = 2
    else:
        name = 'gail'
        horizon = float('inf')

    for _ in range(n_exps):
        agent = PPO(actor=ActorNet(model=SimpleNN(in_dim=3, out_dim=1, n_layers=(64, 64))),
                    critic=CriticNet(model=SimpleNN(in_dim=3, out_dim=1, n_layers=(64, 64))),
                    ckpt_f='../rl_algo/ppo_model/agent_' + name, lr=0.00003, init_explore_noise=0.0, n_explore_iter=10)

        discriminator = SimpleNN(in_dim=4, out_dim=1, n_layers=(64, 64), out_act='sigmoid')
        gail = GAIL(env=env, agent=agent, expert=expert, discriminator=discriminator)
        train_hist = gail.train_model(max_epochs=3000, train_dis_steps=10, train_policy_steps=10, lr_dis=0.0003, verbose=True, horizon=horizon)
        with open('res_' + name + '_1', "a") as f:
            np.savetxt(f, train_hist, delimiter=',')


if __name__ == "__main__":
    # experiment()
    expert = PPO(actor=ActorNet(model=SimpleNN(in_dim=3, out_dim=1, n_layers=(64, 64))),
                 critic=CriticNet(model=SimpleNN(in_dim=3, out_dim=1, n_layers=(64, 64))),
                 ckpt_f='../rl_algo/ppo_model/expert')
    expert.load()
    print(expert)
    import matplotlib.pyplot as plt

    gail = np.loadtxt('res_gail', delimiter=',')
    hgail = np.loadtxt('res_hgail', delimiter=',')

    from plot_utils import plot_curves_with_dist
    _, ax = plt.subplots(1,1)
    plot_curves_with_dist(ax, gail, label='gail', color='b')
    plot_curves_with_dist(ax, hgail, label='hgail', color='r')
    plt.show()
