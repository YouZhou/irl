import os, inspect
current_dir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
os.sys.path.insert(0, "..")

from envs import *
from rewards import *
import matplotlib.pyplot as plt

class MaxEntIRL():
    def __init__(self, env, trajs_s, trajs_a):
        """
        :param env: environment obj (see envs.py)
        :param trajs_s: num_demo x timestamps x state_dim
        :param trajs_a: num_demo x (timestamps-1) x action_dim
        """
        self.env = env
        self.trajs_s = trajs_s
        self.trajs_a = trajs_a
        self.f_expert = env.f_trajs(trajs_s, trajs_a)
        self.f_T = self.env.f_T
        self.f = self.env.f

    def backward_pass(self):
        action_set = self.env.action_set
        num_action = len(action_set)
        state_set = self.env.state_set
        num_state = len(state_set)
        timelength = self.env.timelength

        B_sa = np.zeros(shape=(timelength-1, num_state, num_action))
        B_s = np.zeros(shape=(timelength, num_state))
        B_s[-1, :] = np.exp(np.squeeze(np.array([self.env.reward.evaluate(self.f_T(state)) for state in state_set])))
        for i in range(timelength-2, -1, -1):
            for ai in range(len(action_set)):
                for si in range(len(state_set)):
                    s_t = state_set[si]
                    a_t = action_set[ai]
                    s_t1 = self.env.expect_s(s_t, a_t, B_s[i+1,:])
                    B_sa[i, si, ai] = np.exp(self.env.reward.evaluate(self.f(s_t, a_t))) * s_t1

            B_s[i,:] = np.sum(B_sa[i,:,:], axis=1)

        B_acc = np.tile(np.expand_dims(B_s[0,:],axis=-1), (1,num_action))
        pi_sa = np.divide(B_sa[0,:,:], B_acc+1e-5)
        return pi_sa

    def forward_pass(self, pi_sa):
        action_set = self.env.action_set
        state_set = self.env.state_set
        num_state = len(state_set)
        timelength = self.env.timelength

        A_s = np.zeros(shape=(timelength, num_state))
        A_s[0,:] = 1 / num_state
        for t in range(1, timelength):
            for i in range(len(state_set)):
                for j in range(len(state_set)):
                    for k in range(len(action_set)):
                        s1 = state_set[i]
                        s0 = state_set[j]
                        a0 = action_set[k]
                        A_s[t, i] += A_s[t-1,j] * pi_sa[j, k] * self.env.prob(s1, s0, a0)

        return np.sum(A_s, axis=0)

    def train(self, max_epochs=100):
        f_vec = [self.env.f(s) for s in self.env.state_set]
        f_state = np.stack(f_vec)
        f_exp = np.reshape(self.f_expert, (-1, np.shape(self.f_expert)[-1]))

        for epoch in range(max_epochs):
            pi_sa = self.backward_pass()
            fr_state = self.forward_pass(pi_sa)
            self.env.reward.optimizer.zero_grad()
            rwd_exp = torch.mean(self.env.reward.forward(f_exp))
            fr_t = torch.from_numpy(fr_state).to(device)
            rwd_state = torch.mean(self.env.reward.forward(f_state) * fr_t) #/ torch.sum(fr_state)
            loss = - (rwd_exp - rwd_state)
            loss.backward()
            self.env.reward.optimizer.step()

            print('epoch: {}, loss: {}'.format(epoch, loss))

        rwd = self.env.reward.evaluate(f_state)
        return rwd, f_state, fr_state


if __name__ == "__main__":
    action_set = [np.array([1.0,0.0]), np.array([0.0,1.0])]
    env = Simple2DGridEnv(reward=SimpleNNReward(feat_dim=2, lrate=0.01, layers=[40,1]), timelength=10, gridsize=10, action_set=action_set)
    trajs_s, trajs_a = env.get_expert_trajs()
    for i in range(np.shape(trajs_s)[0]):
        plt.plot(trajs_s[i,:,0], trajs_s[i,:,1], 'b-')

    plt.pause(0.1)

    agent = MaxEntIRL(env,trajs_s,trajs_a)
    rwd, f_state, fr_state = agent.train(max_epochs=100)
    fig = plt.figure()
    ax0 = fig.add_subplot(1,2,1,projection='3d')
    ax1 = fig.add_subplot(1,2,2,projection='3d')
    ax0.plot(f_state[:,0], f_state[:,1], rwd[:,0], 'r.')
    ax1.plot(f_state[:,0], f_state[:,1], fr_state, 'b.')
    plt.show()

