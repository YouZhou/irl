import numpy as np
import gym
import os
from gym.utils import seeding

def get_angle_cos_sin(cs, si):
    th = np.arcsin(si)
    if cs < 0:
        if si > 0:
            th = np.pi - th
        elif si < 0:
            th = -np.pi - th
        else:
            th = th + np.pi

    return th

def angle_normalize(x):
    return (((x+np.pi) % (2*np.pi)) - np.pi)

class PendulumEnv(gym.Env):
    def __init__(self, g=10.0):
        self.max_speed = 8
        self.max_torque = 2.
        self.dt = .05
        self.g = g
        self.m = 1.
        self.l = 1.
        self.action_space = (-self.max_torque, self.max_torque)
        self.act_dim = 1
        self.state_dim = 3
        self.viewer = None

        self.seed()

    def seed(self, seed=None):
        self.np_random, seed = seeding.np_random(seed)
        return [seed]

    def sysfun(self, state, u):
        th, thdot = state

        g = self.g
        m = self.m
        l = self.l
        dt = self.dt

        u = np.clip(u, -self.max_torque, self.max_torque)[0]
        newthdot = thdot + (-3 * g / (2 * l) * np.sin(th + np.pi) + 3. / (m * l ** 2) * u) * dt
        newth = th + newthdot * dt
        newthdot = np.clip(newthdot, -self.max_speed, self.max_speed)
        return np.array([newth, newthdot])

    def get_state(self, obs):
        th = get_angle_cos_sin(obs[0], obs[1])
        th = angle_normalize(th)
        thdot = obs[2]
        return np.array([th, thdot])

    def step(self, u):
        th, thdot = self.state
        self.last_u = u
        u = np.clip(u, -self.max_torque, self.max_torque)[0]
        costs = angle_normalize(th) ** 2 + .1 * thdot ** 2 + .001 * (u ** 2)
        self.state = self.sysfun(self.state, u)
        return self._get_obs(self.state), -costs, self.is_done(), {}

    def reset(self):
        high = np.array([np.pi, 1])
        self.state = self.np_random.uniform(low=-high, high=high)
        self.last_u = None
        return self._get_obs(self.state)

    def get_reward(self, obs, u):
        th = get_angle_cos_sin(obs[0], obs[1])
        th = angle_normalize(th)
        thdot = obs[2]
        costs = angle_normalize(th) ** 2 + .1 * thdot ** 2 + .001 * (u ** 2)
        return -costs

    def is_done(self, steps, obs=None):
        if steps > 200:
            return True
        else:
            return False

    def _get_obs(self, state):
        theta, thetadot = state
        return np.array([np.cos(theta), np.sin(theta), thetadot])

    def sample_expert_state(self):
        pass

    def render(self, mode='human'):
        if self.viewer is None:
            from gym.envs.classic_control import rendering
            self.viewer = rendering.Viewer(500, 500)
            self.viewer.set_bounds(-2.2, 2.2, -2.2, 2.2)
            rod = rendering.make_capsule(1, .2)
            rod.set_color(.8, .3, .3)
            self.pole_transform = rendering.Transform()
            rod.add_attr(self.pole_transform)
            self.viewer.add_geom(rod)
            axle = rendering.make_circle(.05)
            axle.set_color(0, 0, 0)
            self.viewer.add_geom(axle)
            fname = os.path.join(os.path.dirname(__file__), "clockwise.png")
            self.img = rendering.Image(fname, 1., 1.)
            self.imgtrans = rendering.Transform()
            self.img.add_attr(self.imgtrans)

        self.viewer.add_onetime(self.img)
        self.pole_transform.set_rotation(self.state[0] + np.pi / 2)
        if self.last_u:
            self.imgtrans.scale = (-self.last_u / 2, np.abs(self.last_u) / 2)

        return self.viewer.render(return_rgb_array=mode == 'rgb_array')






