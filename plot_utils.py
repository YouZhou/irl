import numpy as np
from scipy.signal import savgol_filter

def plot_curves_with_dist(ax, trajs, label="None", color="r", preprocessing=True):

    trjs = []
    for i in range(trajs.shape[0]):
        y = trajs[i,:]
        win_len = 101
        if preprocessing:
            done = False
            while not done and win_len > 0:
                try:
                    y = savgol_filter(y, window_length=win_len, polyorder=3)
                    done = True
                except:
                    win_len -= 10

        trjs.append(y)

    trjs = np.stack(trjs)

    tr_mean = np.mean(trjs, axis=0)
    tr_std = np.std(trjs, axis=0)
    steps = np.linspace(0, trjs.shape[-1], trjs.shape[-1]) * 1024

    line, = ax.plot(steps, tr_mean, label=label, c=color)
    ax.fill_between(steps, tr_mean - 0.3 * tr_std, tr_mean + 0.3 * tr_std, alpha=0.5, facecolor=color)
    return line